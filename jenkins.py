import sqlite3
from jenkinsapi.jenkins import Jenkins
from datetime import datetime


def setup():
    db = sqlite3.connect('jenkinsJobs')

    cursor = db.cursor()
    cursor.execute(
        '''create table if not exists jenkinsjobs (name TEXT, status TEXT, lastcheck DATETIME)''')
    db.commit()

    return db


def saveInDb(db, valuesList):

    sql = ''' INSERT INTO jenkinsjobs values (?, ?, ?)'''

    cursor = db.cursor()
    cursor.executemany(sql, valuesList)
    db.commit()

    cursor.execute("Select * FROM jenkinsjobs")
    rows = cursor.fetchall()

    for row in rows:
        print(row)


def getJobs():
    # assuming there is a jenkins instance running in localhost:8080 with "Allow anonymous read access"
    J = Jenkins('http://localhost:8080')
    tempList = []
    return J.get_jobs()


def saveJobs(db, jobs):
    valuesList = []
    for job_name, job_instance in jobs:

        name = job_instance.name
        state = 'FINISHED'
        now = datetime.now()
        if job_instance.is_running():
            state = 'RUNNING'

        if not job_instance.is_running() and job_instance.is_enabled():
            state = 'QUEUING'

        else:
            state = 'FINISHED'

        valuesList.append((name, state, now))

    saveInDb(db, valuesList)


db = setup()
jobs = getJobs()

saveJobs(db, jobs)
